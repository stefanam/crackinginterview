package com.stefana.map;

public class Mapa {

    static final int BROJ = 10;
    Unos[] niz = new Unos[BROJ];
    int popunjeno = 0;

    public void put(int key, String value) throws CustomException {

        int index = hash1(key);
        if (popunjeno == BROJ) {
            throw new CustomException();
        }
        if (niz[index] == null) {
            //ta pozicija je prazna, mozemo da trpamo tu
            niz[index] = new Unos(key, value);
        } else {
            //pozivamo sekundarnu hash f-ju dok ne nadjemo slobodnu poziciju
            int count = 0;
            do {
                index = hash2(key, count++);

            } while (niz[index] != null);
            niz[index] = new Unos(key, value);

        }

        popunjeno++;
    }

    public String get(int key) {
        int index = hash1(key);

        if (niz[index] == null) {
            return null;
            //sigurno znamo da nemamo taj element
        }
        if (niz[index].getKey() == key) {
            return niz[index].getValue();
        } else {
            int count = 0;
            do {
                index = hash2(key, count++);

            } while (niz[index] != null && niz[index].getKey() != key);

            if (niz[index] == null) {
                return null;
            } else {
                String value = niz[index].getValue();
                return value;
            }
        }

    }

    public int hash1(int key) {

        return Math.abs(Integer.valueOf(key).hashCode()) % BROJ;
    }

    public int hash2(int key, int callCount) {
        return (Math.abs(Integer.valueOf(key).hashCode()) % 3 + callCount) % BROJ;
    }

}
