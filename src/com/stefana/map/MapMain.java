package com.stefana.map;

public class MapMain {

    public static void main(String[] args) throws CustomException {
        Mapa mapa = new Mapa();
        mapa.put(123, "string 1");
        mapa.put(423, "string 2");
        mapa.get(423);
        mapa.put(552, "string 3");
        mapa.put(765, "string 4");
        mapa.put(12, "string 5");
    }
}
