package com.stefana.map;

public class MyMap<K, V> {

    public Entry<K, V> map[];
    public int capacity = 10;

    public MyMap() {
        map = new Entry[capacity];
    }

    public void insert(K newKey, V newValue) {
        if (newKey == null) {
            return;
        }
        int hash = hash(newKey);
        Entry<K, V> newEntry = new Entry<>(newKey, newValue, null);

        //if table location does not contain any entry, store entry there.
        if (map[hash] == null) {
            map[hash] = newEntry;
        } else {
            Entry<K, V> previous = null;
            Entry<K, V> current = map[hash];

            while (current != null) {  //we have reached last entry of bucket.
                if (current.key.equals(newKey)) {
                    if (previous == null) {   //node has to be insert on first of bucket.
                        newEntry.next = current.next;
                        map[hash] = newEntry;
                        return;
                    } else {
                        newEntry.next = current.next;
                        previous.next = newEntry;
                        return;
                    }
                }

                previous = current;
                current = current.next;
            }

            previous.next = newEntry;
        }
    }

    public boolean delete(K keyToBeDeleted) {

        int hash = hash(keyToBeDeleted);
        if (map[hash] == null) {
            return false;
        } else {
            Entry<K, V> previous = null;
            Entry<K, V> temp = map[hash];

            while (temp != null) {

                if (temp.key.equals(keyToBeDeleted)) {
                    if (previous == null) {
                        map[hash] = map[hash].next;
                        return true;
                    } else {
                        previous.next = temp.next;
                        return true;
                    }

                }

                previous = temp;
                temp = temp.next;
            }
        }
        return false;
    }

    public V get(K key) {
        int hash = hash(key);
        if (map[hash] == null) {
            return null;
        } else {
            Entry<K, V> temp = map[hash];
            while (temp != null) {

                if (temp.key.equals(key)) {
                    return temp.value;
                }
                temp = temp.next;
            }
        }

        return null;
    }

    public int hash(K key) {
        int hash = Math.abs(key.hashCode()) % capacity;
        return hash;
    }

    public int hash2(K key) {
        int hash = Math.abs(key.hashCode()) % 42;
        return hash;
    }

    public void print() {
        for (int i = 0; i < capacity; i++) {
            if (map[i] != null) {
                Entry<K, V> entry = map[i];
                while (entry != null) {
                    System.out.print("{" + entry.key + "=" + entry.value + "}" + " ");
                    entry = entry.next;
                }
            }
        }
    }

}
