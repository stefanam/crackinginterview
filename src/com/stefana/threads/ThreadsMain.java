package com.stefana.threads;

public class ThreadsMain {

    public static void main(String[] args) {

        if (args.length < 2){
            System.out.println("Usage: java Simulator <number of readers> <number of writers>");
        } else {
            final int READERS = 1;
            final int WRITERS = 2;
            Database database = new Database();
            for (int i = 0; i < READERS; i++){
                new Reader(database).start();
            }
            for (int i = 0; i < WRITERS; i++){
                new Writer(database).start();
            }
        }
    }
}
