package com.stefana.threads;

public class Writer extends Thread{

    public static int writers = 0;
    private Database database;
    private int number;

    public Writer(Database database) {
        this.database = database;
        this.number = Writer.writers++;
    }

    /**
     Writes.
     */
    @Override
    public void run() {
        while (true){
            final int DELAY = 5000;
//            try {
//                Thread.sleep((int) Math.random() * 5000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            this.database.write(this.number);
        }
    }
}
