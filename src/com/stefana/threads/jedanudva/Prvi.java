package com.stefana.threads.jedanudva;

public class Prvi extends Thread {


    public void run() {
        //while (true) {

                System.out.println("Pokrenut prvi tred...");

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //synchronized (this) {
                Drugi drugi = new Drugi();
                drugi.start();

                //odricemo se procesora da bi neki drugi tred uzeo procesor da radi nesto na njemu
                yield();

        try {
            //sacekaj da drugi tread zavrsi
            drugi.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Zavrsen prvi tred...");
        //}
        //}
    }

}
