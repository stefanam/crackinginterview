package com.stefana.threads.notifikacije;

public class Main {

    public static int vrednost = 0;

    public static void main(String[] args) {

        Object sync = new Object();

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (sync) {
                    while (vrednost < 2) {
                        try {
                            sync.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("Jedan: vrednost je dva: " + vrednost);
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (sync) {
                    while (vrednost < 2) {
                        try {
                            sync.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("Dvojka: vrednost je dva: " + vrednost);
                }

            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (sync) {
                    vrednost = 1;
                    sync.notifyAll();
                    vrednost = 2;
                    sync.notifyAll();
                }
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
