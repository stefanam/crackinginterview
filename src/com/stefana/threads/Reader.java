package com.stefana.threads;

public class Reader extends Thread{

    public static int readers = 0; //number of readers
    private Database database;
    private int number;

    /**
     Creates a Reader for the specified database.

     @param database database from which to be read.
     */
    public Reader(Database database) {
        this.database = database;
        this.number = Reader.readers++;
    }

    /**
     Reads.
     */

    @Override
    public void run() {
        while (true){
            final int DELAY = 5000;
//            try {
//                Thread.sleep((int)Math.random() * DELAY);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

            this.database.read(this.number);
        }
    }
}
