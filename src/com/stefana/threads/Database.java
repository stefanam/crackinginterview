package com.stefana.threads;

import java.util.concurrent.locks.ReentrantLock;

public class Database {
    ReentrantLock lock = new ReentrantLock();
    private int readers;

    public Database() {
        this.readers = 0;
    }

    /**
     Read from this database.

     @param number Number of the reader.
     */
    public void read(int number){
      // synchronized (this){
        lock.lock();
            this.readers++;
            System.out.println("Reader " + number + " starts reading.");
    //    }
        lock.unlock();

//        final int DELAY = 5000;
//        try {
//            Thread.sleep((int) (Math.random() * DELAY));
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

     //   synchronized (this){
        lock.lock();
            System.out.println("Reader " + number + " stops reading.");
            this.readers--;
            if (this.readers == 0){
//                this.notifyAll();
            }
      //  }
        lock.unlock();
}
    /**
     Writes to this database.

     @param number Number of the writer.
     */
    public void write(int number){
        lock.lock();
        while (this.readers != 0){
//            try {
//                this.wait();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

            System.out.println("Writer " + number + " starts writing.");
//            final int DELAY = 5000;
//            try {
//                Thread.sleep((int)Math.random() * DELAY);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            System.out.println("Writer " + number + " stops writing.");
         //   this.notifyAll();
        }
        lock.unlock();
    }
}
