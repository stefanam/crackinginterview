package com.stefana.inheritance;

public abstract class Car {

    private String type;
    private String model;
    private String fuelType;
    private double engine;
    static  Car car;

    public Car(String type, String model, String fuelType, double engine) {
        this.type = type;
        this.model = model;
        this.fuelType = fuelType;
        this.engine = engine;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public double getEngine() {
        return engine;
    }

    public void setEngine(double engine) {
        this.engine = engine;
    }

    public void start(){
        System.out.println("Starting " + getType() + " " + getModel() + " car.");
    }

    public void stop(){
        System.out.println("Stopping " + getType() + " " + getModel() + " car.");
    }

    public abstract String paint();

    public static void sth(){
        car.start();
        System.out.println("Sth");
    }
}
