package com.stefana.inheritance;

public class Skoda extends Car implements NekiInterfejs{

    private int year;
    private int stateOfProduce;
    Integer num = new Integer("55");


    public Skoda(String type, String model, String fuelType, double engine, int year, int stateOfProduce) {
        super(type, model, fuelType, engine);
        this.year = year;
        this.stateOfProduce = stateOfProduce;
    }

    @Override
    public void start() {
        super.start();
        System.out.println("Skodilak started");
    }

    @Override
    public void stop() {
        super.stop();
        System.out.println("Skodilak stopped");
    }
    //abstract method
    @Override
    public String paint() {
        return "Painting " + getModel() + " " + getType() + " in some nice color...";
    }

    @Override
    public void prvaMetoda() {
       // varijabla1 = varijabla1 + 6; ne moze jer su sve varijable u interfejsu po difoltu final
    }

    @Override
    public void drugaMetoda() {

    }
}
