package com.stefana.inheritance;

public class MainCar {

    public static void main(String[] args) {

        //Car fordcar = new Car("Ford", "Focus", "Disel", 1.8);
        //ne moze car da se inicijalizuje jer je apstraktna klasa sad

        Ford ford = new Ford("Ford", "Focus", "Disel", 1.8, 2002);

        Car car = ford;  //dodela referenci base klase izvedenu klasu
        car.start();
        ((Ford) car).drive(40, 2);
        //paint je apstraktna metoda
        System.out.println(car.paint());

        String s = "value1";
        s.concat("v");
        System.out.println(s);  //posto je string immutable vrednost value1 je nepromenjena

        String c = s.concat("value2");
        System.out.println(c);   //da bi smo ovo izveli moramo da dodelimo ovu vrednost novom objektu
    }
}
