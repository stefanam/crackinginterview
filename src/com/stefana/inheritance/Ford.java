package com.stefana.inheritance;

public class Ford extends Car {

    private int year;

    public Ford(String type, String model, String fuelType, double engine, int year) {
        super(type, model, fuelType, engine);
        this.year = year;
    }



    @Override
    public void start() {
        super.start();
        System.out.println("Ford, best car ever just started to rock...");
    }


    @Override
    public void stop() {
        super.stop();
        System.out.println("Ford, best car ever just went to rest...");
    }
    public void drive(int speed){
        System.out.println("Driving " + speed + " km/h");
    }

    public void drive(int speed, int acceleration){
        System.out.println("Driving " + speed + " km/h, acceleration started for " +
                acceleration + " % from current speed...");
    }

    //abstract method
    @Override
    public String paint() {
        return "Painting " + getModel() + " " + getType() + " in some nice color...";
    }


}
