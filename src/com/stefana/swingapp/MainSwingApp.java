package com.stefana.swingapp;

import javax.swing.*;
import javax.swing.text.TextAction;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainSwingApp {


    public static void main(String[] args) {
        JFrame jFrame = new JFrame();


        jFrame.setVisible(true);
        JButton button = new JButton();
        button.setText("Click");
        jFrame.add(button, BorderLayout.PAGE_START);

        JTextField textField = new JTextField();
        textField.setToolTipText("Put some words here...");
        jFrame.add(textField, BorderLayout.PAGE_END);


        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (textField.getText().isEmpty()) {

                    JOptionPane.showMessageDialog(jFrame, " You have to put some text in text field...");
                }
                String text = textField.getText().toUpperCase();
                textField.setText(text);

            }
        });
        jFrame.setSize(500, 500);

    }


}
