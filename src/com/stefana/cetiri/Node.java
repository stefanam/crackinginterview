package com.stefana.cetiri;

public class Node {

    int vrednost;
    Node left;
    Node right;

    public Node(int vrednost, Node left, Node right) {
        this.vrednost = vrednost;
        this.left = left;
        this.right = right;
    }

    public Node() {
    }

    public Node(int vrednost) {
        this.vrednost = vrednost;
    }

    public int getVrednost() {
        return vrednost;
    }

    public void setVrednost(int vrednost) {
        this.vrednost = vrednost;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}
