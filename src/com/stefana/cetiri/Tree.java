package com.stefana.cetiri;

import java.util.Stack;

public class Tree {

    public Node rootNode;

    public void add(int value) {
        Node current = rootNode;
        if (rootNode == null) {
            rootNode = new Node(value);
        } else {
            while (true) {
                if (current.vrednost < value) {
                    if (current.right == null) {
                        current.right = new Node(value);
                        break;
                    } else {
                        current = current.right;
                    }
                } else {
                    if (current.left == null) {
                        current.left = new Node(value);
                        break;
                    } else {
                        current = current.left;
                    }
                }
            }
        }
    }

    public static int maxDepth(Node rootNode) {
        if (rootNode == null) {
            return 0;
        }
        return 1 + Math.max(maxDepth(rootNode.left), maxDepth(rootNode.right));
    }

    public static int minDepth(Node rootNode) {
        if (rootNode == null) {
            return 0;
        }

        return 1 + Math.min(minDepth(rootNode.left), minDepth(rootNode.right));
    }

    public static boolean isBalanced(Node rootNode) {
        return (maxDepth(rootNode) - minDepth(rootNode)) <= 1;
    }

    public void inOrderTraversal(Node thisNode) {

        if (thisNode == null) {
            return;
        }
        inOrderTraversal(thisNode.left);
        System.out.print(thisNode.vrednost + " ");
        inOrderTraversal(thisNode.right);

    }

    public void preOrderTraversal(Node thisNode) {
        if (thisNode == null) {
            return;
        }
        System.out.print(thisNode.vrednost + " ");
        preOrderTraversal(thisNode.left);
        preOrderTraversal(thisNode.right);
    }

    public void postOrderTraversal(Node thisNode) {
        if (thisNode == null) {
            return;
        }

        postOrderTraversal(thisNode.left);
        postOrderTraversal(thisNode.right);
        System.out.print(thisNode.vrednost + " ");
    }

    public void inOrderTraversalIterative() {
        if (rootNode == null) {
            return;
        }

        Stack<Node> stack = new Stack<>(); //tu cuva sve sto obilazi na tekucoj iteraciji
        Node current = rootNode;

        while (current != null || stack.size() > 0) {

            while (current != null) {
                stack.push(current);
                current = current.left;
            }
            current = stack.pop();
            System.out.print(current.vrednost + " ");
            current = current.right;
        }
    }

    public void preOrderTraversalIterative() {
        if (rootNode == null)
            return;

        Stack<Node> stack = new Stack<Node>();
        Node current = rootNode;


        stack.push(current);


        while (current != null && stack.size() > 0) {

            current = stack.pop();
            System.out.print(current.vrednost + " ");

            Node rightSubtree = current.right;
            while (rightSubtree != null) {
                stack.push(rightSubtree);
                rightSubtree = rightSubtree.right;
            }
            Node leftSubtree = current.left;
            while (leftSubtree != null) {
                stack.push(leftSubtree);
                leftSubtree = leftSubtree.left;
            }
        }
    }

    public void postOrderTraversalIterative() {
        Stack<Node> S = new Stack<Node>();

        // Check for empty tree
        if (rootNode == null)
            return;
        S.push(rootNode);
        Node prev = null;
        while (!S.isEmpty()) {
            Node current = S.peek();

            /* go down the tree in search of a leaf an if so process it
            and pop stack otherwise move down */
            if (prev == null || prev.left == current ||
                    prev.right == current) {
                if (current.left != null)
                    S.push(current.left);
                else if (current.right != null)
                    S.push(current.right);
                else {
                    S.pop();
                    System.out.print(current.vrednost + " ");
                }

                /* go up the tree from left node, if the child is right
                   push it onto stack otherwise process parent and pop
                   stack */
            } else if (current.left == prev) {
                if (current.right != null)
                    S.push(current.right);
                else {
                    S.pop();
                    System.out.print(current.vrednost + " ");
                }

                /* go up the tree from right node and after coming back
                 from right node process parent and pop stack */
            } else if (current.right == prev) {
                S.pop();
                System.out.print(current.vrednost + " ");
            }

            prev = current;
        }
    }
}
