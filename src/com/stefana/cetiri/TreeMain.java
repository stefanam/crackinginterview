package com.stefana.cetiri;

public class TreeMain {

    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.add(3);
        tree.add(5);
        tree.add(1);
        tree.add(2);
        tree.add(4);
        tree.inOrderTraversal(tree.rootNode);
        System.out.println();
        tree.preOrderTraversalIterative();
        System.out.println();
        tree.postOrderTraversalIterative();
    }
}
