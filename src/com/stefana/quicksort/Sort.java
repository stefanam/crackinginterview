package com.stefana.quicksort;

public class Sort {

    public static void main(String[] args) {

        int[] niz =  {24,2,45,20,56,75,2,56,99,53,12};
        quickSort(niz, 0, niz.length - 1);
        for (int i:niz){
            System.out.print(i + " ");
        }

    }

    public static void quickSort(int[] niz, int lower, int higher){

        if (niz == null || niz.length == 0){
            return;
        }
        int i = lower;
        int j = higher;
        int pivot = niz[lower+(higher-lower)/2];

        while (i <= j){

            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a number
             * from right side which is less then the pivot value. Once the search
             * is done, then we exchange both numbers.
             */

            while (niz[i] < pivot){
                i++;  //da prolazi kroz levi deo niza
            }

            while (niz[j] > pivot){
                j--;  //da prolazikroz desni deo niza
            }

            if (i <= j){  // ako je br iz levog dela niza manji od desnog ih da zameni
                int temp = niz[i];  //exchange numbers
                niz[i] = niz[j];
                niz[j] = temp;
                i++;
                j--;
            }
        }

        //poziva se rekurzivno

        if (lower < j){
            quickSort(niz, lower, j);
        }
        if (i < higher){
            quickSort(niz, i, higher);
        }
    }
}
