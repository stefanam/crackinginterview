package com.stefana.quicksort;

public class MyQuickSort {

    public static void main(String[] args) {

        int[] niz = {0, 55, 23, 123, 90, 44, 67, 92, 33, 32, 11, 21};
        quickSort(niz, 0, niz.length - 1);
        for (int i:niz){
            System.out.print(i + " ");
        }
    }

    public static void quickSort(int[] niz, int lower, int higher){

        if (niz == null || niz.length == 0){
            return;
        }
        int i = lower;
        int j = higher;
        int pivot = niz[lower + (higher - lower) / 2];

        while (i <= j){
            while (niz[i] < pivot){
                i++;
            }
            while (niz[j] > pivot){
                j--;
            }
            if (i<= j){
                int temp = niz[i];
                niz[i] = niz[j];
                niz[j] = temp;
                i++;
                j--;
            }
        }

        if (lower < j){
            quickSort(niz, lower, j);
        }

        if (i < higher){
            quickSort(niz, i, higher);
        }
    }
}
