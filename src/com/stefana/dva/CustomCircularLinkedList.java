package com.stefana.dva;

public class CustomCircularLinkedList extends CustomLinkedList {


    CircularNode head = null;
    CircularNode tail = null;
    private int numOfNodes = 0;
    CircularNode loopBegginer;

    class CircularNode {

        CircularNode next;
        int data;

        public CircularNode(int data) {
            this.data = data;
        }
    }


    public void appendToHead(int data) {
        CircularNode node = new CircularNode(data);

        if (numOfNodes == 0) {
            head = node;
            tail = node;
            node.next = head;
        }

        CircularNode current = head;
        node.next = current;
        head = node;

        loopBegginer = findNode(numOfNodes / 2);
        tail.next = loopBegginer;
        numOfNodes++;
    }

//    public boolean hasLoop(CustomCircularLinkedList list){
//
//        if (tail != null){
//            return true;
//        }
//
//        return false;
//    }

    public CircularNode getHeadNode() {

        return head;
    }

    public boolean hasLoop1(CircularNode first){
                if(first == null) // list does not exist..so no loop either
            return false;

        CircularNode slow, fast; // create two references.

        slow = fast = first; // make both refer to the start of the list

        while(true) {

            slow = slow.next;          // 1 hop

            if(fast.next != null)
                fast = fast.next.next; // 2 hops
            else
                return false;          // next node null => no loop

            if(slow == null || fast == null) // if either hits null..no loop
                return false;

            if(slow == fast) // if the two ever meet...we must have a loop
                return true;
    }
    }

    public  CircularNode hasLoop2(CircularNode first){

        CircularNode node1 = head;
        CircularNode node2 = head;

        // Find meeting point
        while (node2.next != null) {
            node1 = node1.next;
            node2 = node2.next.next;
            if (node1 == node2) {
                break;
            }
        }

        // Error check - there is no meeting point, and therefore no loop
        if (node2.next == null) {
            return null;
        }
        // Move n1 to Head. Keep n2 at Meeting Point. Each are k steps
          //from the Loop Start. If they move at the same pace, they must
              //meet at Loop Start.
        node1 = head;
        while (node1 != node2){
            node1 = node1.next;
            node2 = node2.next;
        }
        // Now n2 points to the start of the loop.
        return node2;

}

    public void appendToTail(int data) {
        if (numOfNodes == 0 && numOfNodes == 1) {
            appendToHead(data);
        } else {
            CircularNode node = new CircularNode(data);
            tail.next = node;
            tail = node;
            loopBegginer = findNode(numOfNodes / 2);
            tail.next = loopBegginer;
            numOfNodes++;
        }


    }

    public void deleteFirst() {
        if (numOfNodes == 0) {
            System.out.println("List is empty...");
        }
        head = head.next;
        loopBegginer = findNode(numOfNodes / 2);
        tail.next = loopBegginer;
        numOfNodes--;
    }

    public CircularNode findNode(int index) {
        CircularNode current = head;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }

        return current;
    }


    public int getSize() {
        return numOfNodes;
    }

    public void print() {
        CircularNode current = head;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }
}
