package com.stefana.dva;

public class CustomLinkedList {

    Node head;
    private int numOfNodes = 1;

    class Node{
       Node next = null;
        int data;

        public Node(int data) {
            this.data = data;
        }
    }

    public void appendToTail(int data){

        if (head == null){
            head = new Node(data);
        } else {
            Node current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = new Node(data);
            numOfNodes++;
        }
    }

    public void appendToHead(int data){

        Node current = head;
        head = new Node(data);
        head.next = current;
        numOfNodes++;

    }

    public void addAtIndex(int index, int data){

        Node current = head;
        Node newNode;
        for (int i = 0; i < index - 1 && current.next != null; i++){
            current = current.next;
        }

        newNode = current.next;
        current.next = new Node(data);
        current.next.next = newNode;
        numOfNodes++;
    }

    public void deleteAtIndex(int index){
        Node current = head;

        for (int i = 0 ; i < index - 1 && current.next != null; i++){
            current = current.next;
        }

        current.next = current.next.next;
        numOfNodes--;

    }
    public void deleteFirst(){
        head = head.next;
        numOfNodes--;
    }

    public void deleteLast(){
        Node previous = null;
        Node current = head;
        while (current.next != null){
            previous = current;
            current = current.next;
        }
        previous.next = null;
        numOfNodes--;

    }

    public void deleteNode(int data){
        Node current = head;
        while (current.next.data != data){
                current = current.next;
        }

        current.next = current.next.next;
        numOfNodes--;

    }

    public int find(int index){
        Node current = head;
        for (int i = 0; i < index; i++){
            current = current.next;
        }

        return current.data;
    }

    public int getSize(){
        return numOfNodes;
    }

    public void print(){
        Node current = head;
        while (current != null){
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }

    public void removeDuplicates(){

        Node current = head;

        for (int i = 0; i < numOfNodes; i++){
            if (current.data == current.next.data){
                deleteAtIndex(i);
            }
            current = current.next;
        }
    }

    public void getNthToLastElement(int index){
        Node current = head;
        for (int i = 0; i < index; i++){
            current = current.next;
        }

        for (int i = index; i < numOfNodes - 1; i++){
            while (current != null){
            System.out.print(current.data + " ");
            current = current.next;
            }
        }
    }

    public int sumValueOfNodes(){
        Node current = head;
        int sum = 1;
        for (int i = 0; i < numOfNodes - 1; i++){
            current = current.next;
            sum += current.data;
        }
        return sum;
    }

    public CustomLinkedList makeFromSums(CustomLinkedList... lists){
        CustomLinkedList ll = new CustomLinkedList();



        for (CustomLinkedList list:lists){
            int sum = list.sumValueOfNodes();
            ll.appendToHead(sum);
        }

        ll.print();
        return ll;
    }


}
