package com.stefana.javainterviewquestions.varargspr;

public class VarArgsPr {

    public static void main(String[] args) {

        System.out.println(sum(2, 2, 20, 6));
    }

    public static int sum(int... numbers){
        int sum = 0;
        for (int number:numbers){
            sum += number;
        }

        return sum;
    }
}
