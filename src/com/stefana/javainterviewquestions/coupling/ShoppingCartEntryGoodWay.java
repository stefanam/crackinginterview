package com.stefana.javainterviewquestions.coupling;

public class ShoppingCartEntryGoodWay {
    float price;
    int quantity;

    public float getTotalPrice() {
        return price * quantity;
    }
}

class CartContents {

    ShoppingCartEntryGoodWay[] items;

    public float getTotalPrice() {

        float totalPrice = 0;
        for (ShoppingCartEntryGoodWay item : items) {
            totalPrice += item.getTotalPrice();
        }
        return totalPrice;
    }

class Order {
    private CartContents cart;
    private float salesTax;

    public Order(CartContents cart, float salesTax) {
        this.cart = cart;
        this.salesTax = salesTax;
    }

    public float totalPrice() {
        return cart.getTotalPrice() * (1.0f + salesTax);
    }
}
}
