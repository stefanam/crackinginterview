package com.stefana.javainterviewquestions.collections;

public class Player implements Comparable<Player>{

   private String name;
   private int bodovi;

    public Player(String name, int bodovi) {
        this.name = name;
        this.bodovi = bodovi;
    }


    @Override
    public int compareTo(Player player) {

        if (this.bodovi > player.bodovi){
            return 1;
        }
        if (this.bodovi < player.bodovi){
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return name + ", bodova " + bodovi;
    }
}
