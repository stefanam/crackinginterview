package com.stefana.javainterviewquestions.collections;

import java.util.HashSet;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class SetPrimer {

    public static void main(String[] args) {
        SortedSet set = new TreeSet();
        set.add(3);
        set.add(1);
        set.add(4);
        set.add(2);

        for (Object i :set){
            System.out.print(i + " ");
        }

        System.out.println();

        NavigableSet set2 = new TreeSet();
        set2.add(3);
        set2.add(1);
        set2.add(4);
        set2.add(2);

        for (Object i :set2){
            System.out.print(i + " ");
        }
        System.out.println();

        System.out.println(set2.ceiling(1));
        System.out.println(set2.pollLast());
        System.out.println("Higher than 2 in this set is " + set2.higher(2));

        System.out.println();

        TreeSet<Integer> set1 = new TreeSet<>();
        set1.add(3);
        set1.add(4);
        set1.add(5);
        set1.add(1);
        set1.add(2);
        System.out.println(set1.floor(4));
        System.out.println(set1.ceiling(4));
        System.out.println(set1.higher(4));
        System.out.println(set1.lower(4));
    }
}
