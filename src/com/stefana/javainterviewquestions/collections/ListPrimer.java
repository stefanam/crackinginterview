package com.stefana.javainterviewquestions.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ListPrimer {

    public static void main(String[] args) {

        List<Integer> lista = new ArrayList<>();
        lista.add(2);
        lista.add(5);
        lista.add(7);
        lista.add(1);
        lista.add(9);
        lista.add(6);
        for (Integer i : lista){
            System.out.print(i + " ");
        }
        System.out.println();

        List<Integer> lista2 = new ArrayList<>();
        lista2.add(10);
        lista2.add(54);
        lista2.add(77);
        lista2.add(1);
        lista2.add(9);
        lista2.add(6);
        for (Integer i : lista2){
            System.out.print(i + " ");
        }
        System.out.println();
       lista2.retainAll(lista);

        for (int i : lista2){
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println(lista.get(5));

        Iterator<Integer> listIterator = lista.iterator();
        while (listIterator.hasNext()){
            Integer i = listIterator.next();
            System.out.print(i + " ");
        }
        System.out.println();
        Collections.sort(lista);
        for (int i : lista){
            System.out.print(i + " ");
        }

        System.out.println("Comparable....");
        Player p1 = new Player("Oto", 234);
        Player p2 = new Player("Logo", 908);
        Player p3 = new Player("Sparrow", 123);
        Player p4 = new Player("Airplane", 634);
        List<Player> players = new ArrayList<>();
        Collections.sort(players);
        for (Player player : players){
            System.out.print(player + " ");
        }

        System.out.println("Comparable....");
        PlayerWithoutComparable p11 = new PlayerWithoutComparable("Oto", 234);
        PlayerWithoutComparable p22 = new PlayerWithoutComparable("Logo", 908);
        PlayerWithoutComparable p33 = new PlayerWithoutComparable("Sparrow", 123);
        PlayerWithoutComparable p44 = new PlayerWithoutComparable("Airplane", 634);
        List<PlayerWithoutComparable> players2 = new ArrayList<>();
    //    Collections.sort(players2); ne moze
        for (Player player : players){
            System.out.print(player + " ");
        }
    }
}
