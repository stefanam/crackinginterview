package com.stefana.javainterviewquestions.collections;

import java.util.HashMap;
import java.util.Map;

public class MapPrimer {



    public static void main(String[] args) {
        Map<String, Player> map = new HashMap<>();
        map.put("neko", new Player("neko", 123));
        map.put("tamo", new Player("tamo", 332));
        map.put("koka", new Player("koka", 897));
        map.put("pevac", new Player("pevac", 345));
        System.out.println(map.get("koka").toString());
        for (String name: map.keySet()){

            String key = name.toString();
            String value = map.get(name).toString();
            System.out.println(" key: " + key + " | value: " + value);


        }

    }

}
