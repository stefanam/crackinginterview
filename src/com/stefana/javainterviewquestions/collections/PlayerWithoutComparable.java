package com.stefana.javainterviewquestions.collections;

public class PlayerWithoutComparable {

    private String name;
    private int bodovi;

    public PlayerWithoutComparable(String name, int bodovi) {
        this.name = name;
        this.bodovi = bodovi;
    }
}
