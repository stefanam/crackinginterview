package com.stefana.javainterviewquestions.threads;

import java.util.concurrent.Callable;

public class CallablePrimer implements Callable<String> {

    @Override
    public String call() throws Exception {
        return "result....";
    }
}
