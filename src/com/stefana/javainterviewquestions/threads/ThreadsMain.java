package com.stefana.javainterviewquestions.threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadsMain {

    public static void main(String[] args) {

        //Ovako pokrecempo tred kada nasa klasa extenduje Thread
        BattingStatistics tred1 = new BattingStatistics();
        tred1.start();

        //Ovako pokrecemo tred kada nasa klasa implementira runnable interfejs
        BowingStatistics tred2 = new BowingStatistics();
        Thread tred = new Thread(tred2);
        tred.start();
//        try {
//            tred.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        //  tred2.run();  //ovo bi se pokrenulo kao regularna metoda



//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        executorService.execute(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("From executor service...");
//            }
//        });
//
//        System.out.println("End of main...");
//        executorService.shutdown();
    }
}
