package com.stefana.javainterviewquestions.threads.synchronizedprimer;

public class SynchronizedMain {

    public static void main(String[] args) {
        Calculator thread = new Calculator();
        synchronized(thread){
            thread.start();
            try {
                thread.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(thread.sumUptoMillion);
    }
}
