package com.stefana.javainterviewquestions.cohesion;

public class DownloadAndStore {

    void downloadFromInternet() {
    }

    void parseData() {
    }

    void storeIntoDatabase() {
    }

    void doEverything() {
        downloadFromInternet();
        parseData();
        storeIntoDatabase();
    }
}
