package com.stefana.javainterviewquestions.cohesion;

public class InternetDownloaderGoodWay {
    public void downloadFromInternet() {
    }
}

class DataParser {
    public void parseData() {
    }
}

class DatabaseStorer {
    public void storeIntoDatabase() {
    }
}

class DownloadAndStoreGoodWay {

    void doEverything() {

        new InternetDownloaderGoodWay().downloadFromInternet();
        new DataParser().parseData();
        new DatabaseStorer().storeIntoDatabase();
    }
}

//svaka klasa ima svoju odgovornost