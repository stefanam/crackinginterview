package com.stefana.javainterviewquestions.initializerexample;

public class InstaceInitializerExample {
    static int count;
    int i;

    {
      //This is an instance initializers. Run every time an object is created.
      //static and instance variables can be accessed
        System.out.println("Instance Initializer");
        i = 6;
        count = count + 1;
        System.out.println("Count when Instance Initializer is run is " + count);
    }

    public static void main(String[] args) {
        InstaceInitializerExample example = new InstaceInitializerExample();
        InstaceInitializerExample example1 = new InstaceInitializerExample();
        InstaceInitializerExample example2 = new InstaceInitializerExample();
    }
}
