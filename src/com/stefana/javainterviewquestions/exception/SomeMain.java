package com.stefana.javainterviewquestions.exception;

public class SomeMain {


    public static void main(String[] args){

        try {
            AmountAdder.addAmounts(new Amount("RUPEE", 5), new Amount("DOLLAR",
                    5));
        } catch (Exception e) {
            System.out.println("Handled Exception");
        }
    }



}
