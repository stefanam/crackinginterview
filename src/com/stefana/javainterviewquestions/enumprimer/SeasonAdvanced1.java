package com.stefana.javainterviewquestions.enumprimer;

public enum  SeasonAdvanced1 {

    WINTER(1), SPRING(2), SUMMER(3), FALL(4);
    private int code;
    SeasonAdvanced1(int i) {
        this.code = i;
    }

    public int getCode() {
        return code;
    }

    public static SeasonAdvanced1 valueOf(int code){
        for (SeasonAdvanced1 s:SeasonAdvanced1.values()){
            if (s.getCode() == code){
                return s;
            }
        }
        throw new RuntimeException("Value not found");
    }
}
