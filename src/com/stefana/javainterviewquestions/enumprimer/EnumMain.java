package com.stefana.javainterviewquestions.enumprimer;

import static com.stefana.javainterviewquestions.enumprimer.Season.*;

public class EnumMain {



    public static void main(String[] args) {
        System.out.println(
        getExpectedMaxTemperature(SUMMER));

        SeasonAdvanced1 seasonAdvanced1 = SeasonAdvanced1.WINTER;
        System.out.println(
        seasonAdvanced1.getCode());
        System.out.println(
        SeasonAdvanced1.valueOf(3));

        SeasonAdvanced2 sa2 = SeasonAdvanced2.SUMMER;
        System.out.println(sa2.getExpectedMaxTemperature());

    }


    public static Season getExpectedMaxTemperature(Season season) {
        switch (season) {
            case WINTER:
                return Season.WINTER;
            case SPRING:
                return Season.SPRING;
            case FALL:
                return Season.FALL;
            case SUMMER:
                return Season.SUMMER;
        }
        return null;
    }
}
