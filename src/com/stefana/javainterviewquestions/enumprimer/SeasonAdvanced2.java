package com.stefana.javainterviewquestions.enumprimer;

public enum SeasonAdvanced2 {
    WINTER(1) {
        public int getExpecterTemperature() {
            return 5;
        }
    },
    SUMMER(3), SPRING(2) {
        public int getExpecterTemperature() {
            return 20;
        }
    },

    FALL(4);
    private int code;

    SeasonAdvanced2(int i) {
        code = i;
    }
    public int getExpectedMaxTemperature() {
        return 10;
    }
}
