package com.stefana.javainterviewquestions.ostalo;

public class EqualsAndhashcode {

    public static void main(String[] args) throws InterruptedException {


        String s1 = new String("nesto");
        String s2 = new String("nesto");

        Integer i = new Integer(23);
        Integer j = new Integer(23);

        System.out.println(s1.equals(s2));  //true
        System.out.println(s1 == s2);       //false
        System.out.println(i.equals(j));    //true

        Client c1 = new Client(1, "Pera Kojot Genije");
        Client c2 = new Client(1, "Pera Kojot Genije");
        System.out.println(c1.equals(c2));  //false

        String string = "Neki string";   //mogu da se stave u isti pool jer nema new
        String string2 = "Neki string";
        System.out.println(string.equals(string2));  //true
        System.out.println(string == string2);  //true

        int jedan = 1;
        int je = 1;
        System.out.println(je == jedan);  //true
        System.out.println(i == j);  //false

        Integer integer = 10;
        Integer integer1 = 10;
        System.out.println(integer.equals(integer1));  //true
        System.out.println(integer == integer1);  //true

        Object o = new String("bla bla");

        double broj  = 3.6;
        double br2 = 3.4;

        System.out.println("\n"+Math.floor(broj));
        System.out.println("\n"+Math.floor(br2));// suprotno od ceil, zaokruzuje na prvi manji br a ceil na prvi veci

        NijeTred tred = new NijeTred();
        new Thread(tred).start();

        JesteTred tred1 = new JesteTred();
        tred1.start();


    }
}
