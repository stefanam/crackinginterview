package com.stefana.javainterviewquestions.ostalo;

public interface NekiInterfejs {

    int number = 25;

    default void metoda(){
        System.out.println("Neka metoda...");
    }

    void metoda2();
}
