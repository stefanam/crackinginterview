package com.stefana.javainterviewquestions.ostalo;

class Animal {

    //String name;

//    public Animal() {
//        this.name = "Default Name";
//    }
//
//    // This is called a one argument constructor.
//    public Animal(String name) {
//        this.name = name;
//    }

    public void nekaMetod(){
        System.out.println("Nova metoda");
    }

    public String shout(){
        return "I don't know";
    }
    public static void main(String[] args) {

//        Animal animal = new Animal();
//        animal.nekaMetod();
//        System.out.println(animal.shout());
//
//        Animal animal1 = new Dog();
//        System.out.println(animal1.shout());
//
//        Animal animal2 = new Cat();
//        ((Cat) animal2).drugaMetoda();
//
//        Cat cat = new Cat();
//        System.out.println(cat.shout());
//        System.out.println(animal1 instanceof Animal);
//        System.out.println(cat instanceof Cat);
//        System.out.println(cat instanceof Animal);
//        System.out.println(cat instanceof nekiInterfejs);
//        cat.nekaMetod();
//
//        cat.pet(20);

        Animal malaMaca = new MalaMaca();
        System.out.println(malaMaca.shout());

    }

}

interface nekiInterfejs{

}

class Cat extends Animal implements nekiInterfejs{

    @Override
    public String shout() {
        return "Meow Meow";

    }

    public void pet(final int minutes){

        System.out.println("Petted for " + minutes + " minutes.");
    }

    public void drugaMetoda(){
        System.out.println("Ovo je druga metoda");
    }
}

class MalaMaca extends Cat{

}

class Dog extends Animal{
    @Override
    public String shout() {
        return "Bow Bow";
    }

    public void run(){}
}

