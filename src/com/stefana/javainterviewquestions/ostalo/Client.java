package com.stefana.javainterviewquestions.ostalo;

class  Client implements NekiInterfejs {

    private int id;
    private String name;

    public Client(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public void metoda() {
        System.out.println("Ovo ne mora da se overriduje...");
        String a;
    }

    @Override
    public void metoda2() {
        System.out.println("Ovo mora...");
    }
}
