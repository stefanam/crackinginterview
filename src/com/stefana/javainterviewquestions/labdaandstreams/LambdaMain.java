package com.stefana.javainterviewquestions.labdaandstreams;

import java.util.Arrays;
import java.util.List;

public class LambdaMain {

    public static void main(String[] args) {


        Arrays.stream(new String[]{
                "Ram", "Robert", "Rahim"
        }).filter(s -> (s.startsWith("Ro")))
                .map(String::toLowerCase)
                .sorted()
                .forEach(System.out::println);

        List<Integer> numbers = Arrays.asList(1, 3, 4, 6, 2, 7);
        numbers.stream().distinct().forEach(System.out::println);
    }
}
