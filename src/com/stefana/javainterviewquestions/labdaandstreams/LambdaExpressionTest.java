package com.stefana.javainterviewquestions.labdaandstreams;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;

public class LambdaExpressionTest {


    @Test
    public void sumOfOddNumbers_Usual() {
        List<Integer> numbers = Arrays.asList(1, 3, 4, 6, 2, 7);
        int sum = 0;
        for (int number : numbers)
            if (number % 2 != 0)
                sum += number;
        assertEquals(11, sum);
    }

    @Test
    public void sumOfOddNumbers_FunctionalProgrammingExample(){
        List<Integer> numbers = Arrays.asList(1, 3, 4, 6, 2, 7);
        int sum = numbers.stream().filter(number -> (number % 2 != 0)).reduce(0, Integer::sum);

        assertEquals(11, sum);
    }

    @Test
    public void streamExample_Distinct(){
        List<Integer> numbers = Arrays.asList(1, 3, 4, 6, 2, 7);
        numbers.stream().distinct().forEach(System.out::println);
    }

    @Test
    public void streamExample_Sorted() {
        List<Integer> numbers = Arrays.asList(1, 1, 2, 6, 2, 3);
        numbers.stream().sorted().forEach(System.out::print);
    }

    @Test
    public void streamExample_Collect() {
        List<Integer> numbers = Arrays.asList(1, 3, 4, 6, 2, 7);
        List<Integer> oddNumbers = numbers.stream()
                .filter(number -> (number % 2 != 0))
                .collect(Collectors.toList());
        System.out.println(oddNumbers);
    }
}
