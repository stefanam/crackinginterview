package com.stefana.javainterviewquestions.serializible.hierarchy;

import java.io.Serializable;

public class House implements Serializable {

    transient Wall wall;  //solution 1, make wall be transient
    int number;

    public House(int number) {
        super();
        this.number = number;
    }
}

class Wall implements Serializable{  //solution 2, make wall implements Serializible interface too
    int length;
    int breadth;
    int color;
}
