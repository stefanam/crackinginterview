package com.stefana.javainterviewquestions.serializible;

import java.io.*;

public class SerializibleMain {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        //serijalizacija
        FileOutputStream fileOutputStream = new FileOutputStream("Rectangle.ser");
        ObjectOutputStream objectStream = new ObjectOutputStream(fileOutputStream);
        objectStream.writeObject(new Rectangle(5, 6));
        objectStream.close();

        //deserijalizacija
        FileInputStream fileInputStream = new FileInputStream("Rectangle.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Rectangle rectangle = (Rectangle) objectInputStream.readObject();
        objectInputStream.close();
        System.out.println(rectangle.length);
        System.out.println(rectangle.breadth);
        System.out.println(rectangle.area);
    }
}
