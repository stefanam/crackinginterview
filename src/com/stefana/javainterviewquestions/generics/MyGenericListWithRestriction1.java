package com.stefana.javainterviewquestions.generics;

import java.util.ArrayList;
import java.util.List;

public class MyGenericListWithRestriction1<T extends Number> {

    List<T> values = new ArrayList<>();

    public void add(T value){
        values.add(value);
    }
    public void remove(T value){
        values.remove(value);
    }

    public T get(int index){
        return values.get(index);
    }
}
