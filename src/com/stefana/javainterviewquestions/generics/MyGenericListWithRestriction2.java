package com.stefana.javainterviewquestions.generics;

import java.util.ArrayList;
import java.util.List;

public class MyGenericListWithRestriction2<T extends Number>{

    List<T> values = new ArrayList<>();

    public void add(T value){
        values.add(value);
    }
    public void remove(T value){
        values.remove(value);
    }

    public T get(int index){
        return values.get(index);
    }

    public  <T extends Number> T doSth(T number){
        T result = number;
        return result;
    }

    public void doSth2(List<? super Integer> number){

    }

}
