package com.stefana.javainterviewquestions.generics;

import java.util.ArrayList;
import java.util.List;

public class MyGenericList<T> {

    List<T> values = new ArrayList<>();

    public void add(T value){
        values.add(value);
    }
    public void remove(T value){
        values.remove(value);
    }

    public T get(int index){
        return values.get(index);
    }
}
