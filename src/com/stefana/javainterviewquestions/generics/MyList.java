package com.stefana.javainterviewquestions.generics;

import java.util.ArrayList;
import java.util.List;

public class MyList {

    List<String> values = new ArrayList<>();

    public void add(String value){
        values.add(value);
    }
    public void remove(String value){
        values.remove(value);
    }
}
