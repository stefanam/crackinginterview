package com.stefana.javainterviewquestions.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericsDemoMain {

    public static void main(String[] args) {
        MyList myList = new MyList();
        myList.add("Value 1");
        myList.add("value 2");

        MyGenericList<Integer> lista = new MyGenericList<>();
        lista.add(12);

        MyGenericList<String> listaStringova = new MyGenericList<>();
        listaStringova.add("String 1");

        MyGenericListWithRestriction1<Integer> lista2= new MyGenericListWithRestriction1<>();
      //  MyGenericListWithRestriction1<String> listaneka = new MyGenericListWithRestriction1<>(); ne moze

        MyGenericListWithRestriction2 lista3 = new MyGenericListWithRestriction2();
        lista3.doSth(3);
        List<? super Integer> objects = new ArrayList<>();
        objects.add(4);
        lista3.doSth2(objects);
    }
}
