package com.stefana.tri;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

public class TriTri<T> {

    private Stack<Stack<T>> stacks = new Stack<>();
    private static final int LIMIT = 10;

    public TriTri() {
        stacks.push(new Stack<>());
    }

    public void push(T data) {
        stacks.peek().push(data);
        if (stacks.peek().size() == LIMIT) {
            stacks.push(new Stack<>());
        }
    }

    public T pop() {

        T pop = stacks.peek().pop(); //skidamo sa poslednjeg stega koji je na vrhu element na vrhu

            if (stacks.size() != 1 && stacks.peek().isEmpty()) {
                stacks.pop(); //sad skidamo stek ne element
            }

        return pop;
    }

    public <R extends Collection> R metoda(){
        return (R) new ArrayList<T>();
    }
}
