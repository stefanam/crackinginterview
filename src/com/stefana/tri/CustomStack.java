package com.stefana.tri;

import com.stefana.dva.CustomLinkedList;

public class CustomStack {

    StackNode head;
    StackNode minEl;

    class StackNode {
        StackNode next = null;
        int data;

        public StackNode(int data) {
            this.data = data;
        }
    }

    public void push(int element) {

        StackNode current = new StackNode(element);
        current.next = head;
        head = current;

        //hendlovanej minimuma
        if (minEl == null) {
            //lista je prazna prvi element je ujedno i najmanji
            minEl = head;
        } else {
            //proverimo da li je novi elm ujedno manji
            // od najmanjeg trenutno zabelezebog ako jeste on postaj novi minimum
            if (current.data < minEl.data) {
                minEl = current;
            }
        }

    }

    public StackNode pop() {
        StackNode ret = head;
        head = head.next;

        if (head == null){
            minEl = null;
        } else {
            if(minEl == ret){
                StackNode current = head;
                minEl = head;
                while (current != null){
                    if (current.data < minEl.data){
                        minEl = current;
                    }

                    current = current.next;
                }
            }
        }

        return ret;
    }

    public int peek() {
        return head.data;
    }

    public void print() {
        StackNode current = head;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }

    public StackNode getMin() {
        return minEl;
    }

//    public int minNode(CustomStack lista) {
//        StackNode tekuciMin = new StackNode(head.data);
//        tekuciMin.data = Integer.MAX_VALUE;//fakticki plus beskonacno
//        StackNode tekuci = lista.head;
//
//        while (tekuci != null) {
//
//            if (tekuci.data < tekuciMin.data) {
//                tekuciMin = tekuci;
//            }
//
//            tekuci = tekuci.next;
//        }
//
//        return tekuciMin.data;

}


