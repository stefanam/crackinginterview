package com.stefana.tri;

import java.util.Arrays;
import java.util.Stack;

public class QueueMain {

    public static void main(String[] args) {
//         MyQueue queue = new MyQueue();
//         queue.enqueue(20);
//         queue.enqueue(40);
//        queue.enqueue(50);
//        queue.enqueue(70);
//         queue.print();

//         QueueFromStacks queue1 = new QueueFromStacks();
//         queue1.engueue(4);
//         queue1.engueue(7);
//         queue1.engueue(9);
//        System.out.println(queue1.engueue(6));
//        System.out.println(
//        queue1.dequeue());

        Stack<Integer> stek = new Stack<>();
        stek.push(4);
        stek.push(3);
        stek.push(1);
        stek.push(5);
        stek.push(2);
        printStack(stek);
        System.out.println();
        System.out.println("Sortirani stek...");
        sort(stek);


    }

    public static Stack<Integer> sort(Stack<Integer> stek){
        Stack<Integer> pomocniStek = new Stack<>();
        while (!stek.isEmpty()){
            int temp = stek.pop();

            while (!pomocniStek.isEmpty() && pomocniStek.peek() > temp){
                //pomocniStek.push(stek.pop());
                stek.push(pomocniStek.pop());
            }
            pomocniStek.push(temp);
        }

        return printStack(pomocniStek);
    }

    public static Stack<Integer> sort2(Stack<Integer> stek){
        Stack<Integer> pomocniStek = new Stack<>();
        while(!stek.isEmpty()) {
            int tmp = stek.pop();
            while (!pomocniStek.isEmpty() && pomocniStek.peek() > tmp) {
                stek.push(pomocniStek.pop());  //ako se desi da je prvi u pomocnom
                                               //veci od popovanog u prvom -- vrati ga nazad u prvi stek
            }
            pomocniStek.push(tmp);    //a inace prebacuj iz prvog u drugi stek
        }
         return printStack(pomocniStek);
    }

    public static Stack<Integer> printStack(Stack<Integer> stack) {

        // method 1:
        String values = Arrays.toString(stack.toArray());
        System.out.println(values);

        // method 2:
        Object[] vals = stack.toArray();
        for (Object obj : vals) {
            System.out.print(obj + " ");
        }
        return stack;
    }
}
