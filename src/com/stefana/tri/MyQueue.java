package com.stefana.tri;

public class MyQueue {

    QueueNode head;
    QueueNode tail;

    class QueueNode {
        QueueNode next = null;
        int data;

        public QueueNode(int data) {
            this.data = data;
        }
    }

    public void enqueue(int data) {
        //dodaje vrednost na kraj liste
        QueueNode novi = new QueueNode(data);

        if (head == null) {
            head = novi;
            tail = novi;
        } else {
            tail.next = novi;  //po uslovu ponasanja ove strukture, tail uvek pokazuje na kraj liste,
            // zato sto se pomera na sledecii element sa svakim novim ulancavanjem
            tail = novi;  //prebacimo se na novi element da azuriramo stanje sacelja liste
        }
    }

    public void dequeue() {
        //uzima vrednost sa vrha liste
        head = head.next;
        if (head == null) {
            tail = null;
        }
    }

    public void print() {
        QueueNode current = head;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
        System.out.println();
    }
}
