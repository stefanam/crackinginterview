package com.stefana.jedan.cetiri;

import java.util.Arrays;

public class Jedancetiri {


    public static boolean compare(char[] char1, char[] char2) {

        Arrays.sort(char1);
        Arrays.sort(char2);

        if (char1.length != char2.length) {
            return false;
        }

        for (int i = 0; i < char1.length && i < char2.length; i++) {
            if (char1[i] != char2[i]) {
                return false;
            }

        }

        return true;

    }

    public static boolean compare2(char[] char1, char[] char2) {
        if (char1.length != char2.length) {
            return false;
        }
        int[] counts = new int[64];
        int[] counts2 = new int[64];

        for (int i = 0; i < char1.length; i++) {
            int index = (int) char1[i] - (int) 'A';// ili - 65
            counts[index]++;
        }

        for (int i = 0; i < char2.length; i++) {
            int index = (int) char2[i] - (int) 'A';// ili - 65
            counts2[index]++;
        }

        for (int i = 0; i < counts.length; i++){
            if (counts[i] != counts2[i]){
                return false;
            }
        }

        return true;
    }
}
