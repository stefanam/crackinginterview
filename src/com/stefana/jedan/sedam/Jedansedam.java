package com.stefana.jedan.sedam;

public class Jedansedam {

    public void findAndSetZero(int[][] mat) {

        int[] row = new int[mat.length];
        int[] column = new int[mat[0].length];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                if (mat[i][j] == 0){
                    row[i] = 1;
                    column[j] = 1;
                }
            }
        }

        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                if ((row[i] == 1 || column[j] == 1)) {
                    mat[i][j] = 0;
                }
            }
        }
    }

    public void print(int[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                System.out.print(" " + mat[i][j]);
            }
            System.out.println("");
        }
    }
}
