package com.stefana.jedan.provera;

import java.util.Arrays;
import java.util.Collections;

public class Provera {

    public boolean areUnique(String string) {

        char[] chars = string.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i] == chars[j]) {
                    return false;
                }
            }
        }

        return true;
    }

    public void removeDuplicates(String string) {

        char[] chars = string.toCharArray();
        int temp = 0;

        for (int i = 0; i < chars.length; i++) {
            boolean imaDuplo = false;
            for (int j = 0; j < temp; j++) {
                if (chars[i] == chars[j]) {
                    imaDuplo = true;
                    break;
                }

            }
            if (!imaDuplo) {
                chars[temp] = chars[i];
                temp = temp + 1;
            }
        }
        System.out.println(new String(chars).substring(0, temp));
    }

    public boolean areAnagrams1(String string1, String string2){
        char[] char1 = string1.toCharArray();
        char[] char2 = string2.toCharArray();
        Arrays.sort(char1);
        Arrays.sort(char2);

        if (char1.length != char2.length){
            return false;
        }
        for (int i = 0; i < char1.length; i++){
            if (char1[i] != char2[i]){
                return false;
            }
        }

        return true;
    }

    public boolean areAnagrams2(String string1, String string2){
        char[] char1 = string1.toCharArray();
        char[] char2 = string2.toCharArray();

        if (char1.length != char2.length){
            return false;
        }

        int[] counters = new int[64];

        for (int i = 0; i < char1.length; i++){
            int index = char1[i] - (int)'A';
            counters[index] ++;
        }

        for (int i = 0; i < char2.length; i++){
            int index = char2[i] - (int)'A';
            counters[index] --;
        }

        for (int i = 0; i < counters.length; i++){
            if (counters[i] != 0){
                return false;
            }
        }

        return true;
    }
}
