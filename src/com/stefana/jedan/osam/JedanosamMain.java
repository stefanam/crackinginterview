package com.stefana.jedan.osam;

public class JedanosamMain {

    public static void main(String[] args) {
        Jedanosam jo = new Jedanosam();
        System.out.println(jo.isSubstring("stefana", "ste"));
        System.out.println(jo.isRotation("apple", "ppale"));
    }
}
