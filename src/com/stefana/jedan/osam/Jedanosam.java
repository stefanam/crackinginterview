package com.stefana.jedan.osam;

public class Jedanosam {

    public boolean isSubstring(String string1, String string2){


        char[] char1 = string1.toCharArray();
        char[] char2 = string2.toCharArray();

        if (char2.length >= char1.length){
            return false;
        }

        for (int i = 0; i < char1.length; i++){
            if (char1[i] == char2[0]){
                for (int j = i; j < char2.length; j++){
                    if (char1[i] == char1[j]){
                        return true;
                    }
                }
            }
        }

        return false;

    }

    public boolean isRotation(String string1, String string2){


        if (string1.toCharArray().length == string1.toCharArray().length && string1.toCharArray().length > 0){
            String s1s1 = string1 + string1;
            return isSubstring(s1s1, string2);
        }

        return false;
    }
}
