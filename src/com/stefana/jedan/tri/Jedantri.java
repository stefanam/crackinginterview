package com.stefana.jedan.tri;

public class Jedantri {


    public static String removeDuplicateChars(String string) {
        int position = 0;
        char[] niz = string.toCharArray();
        for (int i = 0; i < niz.length; i++) {
            boolean check = false;
            for (int j = position - 1; j >= 0; j--) {
                if (niz[i] == niz[j]) {
                    check = true;
                    break;
                }
            }

            if (!check) {
                niz[position++] = niz[i];
            }
        }

        return new String(niz).substring(0, position);
    }
}
