package com.stefana.jedan.sest;

public class Jedansest {


    public void rotate90(int[][] mat) {
        for (int i = 0; i < mat.length / 2; i++) {
            for (int j = i; j < mat[i].length - 1 - i; j++) {
                int temp = mat[i][j];
                mat[i][j] = mat[j][mat.length - 1 - i];
                mat[j][mat.length - 1 - i] = mat[mat.length - 1 - i][mat.length - 1 - j];
                mat[mat.length - 1 - i][mat.length - 1 - j] = mat[mat.length - 1 - j][i];
                mat[mat.length - 1 - j][i] = temp;
            }
            System.out.println("");
        }
    }

    public void rotate180(int[][] mat) {
        for (int i = 0; i < mat.length / 2; i++) {
            for (int j = i; j < mat[i].length - 1 - i; j++) {
                int temp = mat[i][j];
                mat[i][j] = mat[j][mat.length - 1 - i];
                mat[j][mat.length - 1 - i] = mat[mat.length - 1 - i][mat.length - 1 - j];
                mat[mat.length - 1 - i][mat.length - 1 - j] = mat[mat.length - 1 - j][i];
                mat[mat.length - 1 - j][i] = temp;

                temp = mat[i][mat.length - 1 - j];
                mat[i][mat.length - 1 - j] = mat[mat.length - 1 - j][mat.length - 1 - i];
                mat[mat.length - 1 - j][mat.length - 1 - i] = mat[i][mat.length - 1 - j];
                mat[i][mat.length - 1 - j] = temp;
            }
            System.out.println("");
        }
    }


    public void print(int[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                System.out.print(" " + mat[i][j]);
            }
            System.out.println("");
        }
    }
}
